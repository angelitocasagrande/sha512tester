﻿using System;
using System.Windows.Forms;

namespace SHA512TesterApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var salt = SaltGenerator.GetSaltString();
            txtSalt.Text = salt;
        }

        private void BtnSHA512_Click(object sender, EventArgs e)
        {
            var salt = txtSalt.Text;
            var pass = txtPassword.Text;
            txtResult.Text = Cryptograph.GetSHA512FromPasswordAndSalt(pass, salt);
        }
    }
}
