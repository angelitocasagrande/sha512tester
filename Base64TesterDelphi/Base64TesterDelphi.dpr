program Base64TesterDelphi;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  System.NetEncoding;

function MAISEncode_HexToString(H: String): String;
var
  i: integer;
begin
  result := '';
  for i := 1 to length(H) div 2 do
    result := result + Char(StrToInt('$' + Copy(H, (i - 1) * 2 + 1, 2)));
end;

function MAISEncode_Crypt(StrSource, StrSalt: String): String;
var
  base64enc: TBase64Encoding;
  textEnc: TEncoding;
  stringToBytes: TBytes;
  i: integer;
  S: String;
begin
  result := '';
  // ENCRYPT: String to Base64 with Salt
  base64enc := TBase64Encoding.Create();
  StrSalt := base64enc.Encode(StrSalt);
  StrSource := base64enc.Encode(StrSource) + StrSalt;
  // ENCRYPT: String to HEX
  textEnc := TEncoding.UTF8;
  stringToBytes := textEnc.GetBytes(StrSource);
  S := '';
  for i := low(stringToBytes) to high(stringToBytes) do
    S := S + IntToHex(stringToBytes[i], 2);
  result := S;
end;

function MAISEncode_Decrypt(StrCrypt, StrSalt: String): String;
var
  base64enc: TBase64Encoding;
  S: String;
begin
  base64enc := TBase64Encoding.Create();
  // DECRYPT: HEX to String
  S := MAISEncode_HexToString(StrCrypt);
  // DECRYPT: Base64 with salt to String
  StrSalt := base64enc.Encode(StrSalt);
  if length(StrSalt) > 0 then
    S := Copy(S, 1, length(S) - length(StrSalt));
  result := base64enc.Decode(S);
end;

var
  salt, msg, salt64, base64, hexMessage, hexReverted, base64Reverted: String;
  stringToBytes: TBytes;
  i, k, c: integer;
  base64enc: TBase64Encoding;
  textEnc: TEncoding;

begin
  try
    salt := '123';
    msg := '12345678';
    c := 0;

    Writeln('Salt: ' + salt);
    Writeln('Message: ' + msg);

    // ************************************

    // Encrypt
    hexMessage := MAISEncode_Crypt(msg, salt);

    // Result
    Writeln('Result: ' + hexMessage);

    // DECRYPT: HEX to String
    base64Reverted := MAISEncode_Decrypt(hexMessage, salt);

    // Reverted
    Writeln('Reverted: ' + base64Reverted);

    sleep(50000);

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.
