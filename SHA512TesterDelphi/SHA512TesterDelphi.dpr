program SHA512TesterDelphi;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  System.Hash;

var
  result, salt, password: String;
  resultByte: TBytes;
  i, k: integer;

begin
  try
    salt := 'S@lt2o19';
    password := '12345678';

    // Hash Bytes
    resultByte := THashSHA2.GetHashBytes(password + salt, SHA512);

    // Byte to HEX
    k := high(resultByte);
    for i := low(resultByte) to k do
      result := result + IntToHex(resultByte[i], 2);

    // Show result
    Writeln('Salt: ' + salt);
    Writeln('Password: ' + password);
    Writeln('Result: ' + result);

    sleep(5000);

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.
