﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Base64TesterApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var salt = SaltGenerator.GetSaltString();
            txtSalt.Text = salt;
        }

        private void BtnBase64_Click(object sender, EventArgs e)
        {
            var msg = txtMessage.Text;
            var salt = txtSalt.Text;


            var base64 = Cryptograph.EncryptBase64WithSalt(msg, salt);
            var hex = Cryptograph.EncryptHEX(base64);

            txtResult.Text = hex;

            var hexReverted = Cryptograph.DecryptHEX(hex);
            var base64Reverted = Cryptograph.DecryptBase64WithSalt(hexReverted, salt);

            txtMessageRevert.Text = base64Reverted;
        }
    }
}
